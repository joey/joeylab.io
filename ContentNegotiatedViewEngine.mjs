class ContentNegotiatedViewEngine {
    constructor(name = "", options = {}){
        this.Name = name
        this.Options = options
        this.path = "something to trick express"
    }
    render(options, fn){
        console.trace('from engine', options, fn.toString())
        fn(null, "just testing man")
    }
}

export default ContentNegotiatedViewEngine