#!/bin/sh

# https://viperstaking.com/ada-tools/node-quickstart
# 2021-06-14 We left off at setting up the config files.

sudo yum update -y
sudo yum install git
sudo amazon-linux-extras install docker
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker ec2-user  # Optional, allows you to run `docker` commands without `sudo`
sudo reboot

git clone https://gitlab.com/viper-staking/cardano-node-setup.git
git clone https://gitlab.com/viper-staking/docker-containers.git
docker pull registry.gitlab.com/viper-staking/docker-containers/cardano-node:latest
