<!doctype html>
<html lang="en">
  <head>
    <meta content="text/html; charset=UTF-8" name="Content-Type"/>
    <meta charset="utf-8"/>
    <base href="../../"/>
    <!--<meta http-equiv="Content-Security-Policy" content="default-src https:">-->

    <title>Flow vs Batch Based Work in Software Development</title>
    <meta name="author" content="Joey Guerra"/>
    <meta name="description" content="In the world of software development, the traditional approach often involves large batches of work, where teams are booked up for months and new features or changes have to wait in line. This approach can lead to long lead times, significant delays, and a constant need for re-prioritization, often involving lengthy discussions with stakeholders."/>
    <meta name="image" content="https://www.joeyguerra.com/imgs/flow-vs-batch-based-work.webp"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="publish_date" property="og:publish_date" content="Mon Aug 12 2024 08:50:00 GMT-0500 (Central Daylight Time)"/>

    <meta property="og:title" content="Flow vs Batch Based Work in Software Development"/>
    <meta property="og:image" content="https://www.joeyguerra.com/imgs/flow-vs-batch-based-work.webp"/>
    <meta property="og:description" content="In the world of software development, the traditional approach often involves large batches of work, where teams are booked up for months and new features or changes have to wait in line. This approach can lead to long lead times, significant delays, and a constant need for re-prioritization, often involving lengthy discussions with stakeholders."/>
    <meta property="og:url" content="https://www.joeyguerra.comblog/2024/flow-vs-batch-based-work.html"/>
    <meta property="og:type" content="summary"/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@ijoeyguerra"/>
    <meta name="twitter:creator" content="@ijoeyguerra"/>
    <meta name="twitter:title" content="Flow vs Batch Based Work in Software Development"/>
    <meta name="twitter:image" content="https://www.joeyguerra.com/imgs/flow-vs-batch-based-work.webp"/>
    <meta name="twitter:description" content="In the world of software development, the traditional approach often involves large batches of work, where teams are booked up for months and new features or changes have to wait in line. This approach can lead to long lead times, significant delays, and a constant need for re-prioritization, often involving lengthy discussions with stakeholders."/>
    
    <link rel="apple-touch-icon" href="favicon_package_v0/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="favicon_package_v0/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="favicon_package_v0/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="favicon_package_v0/favicon-16x16.png"/>
    <link rel="manifest" href="favicon_package_v0/site.webmanifest"/>
    <link rel="mask-icon" href="favicon_package_v0/safari-pinned-tab.svg" color="#5bbad5"/>
    <meta name="msapplication-TileColor" content="#da532c"/>
    <meta name="theme-color" content="#ffffff"/>

    <link rel="shortcut icon" href="favicon_package_v0/favicon.ico"/>

    <link rel="apple-touch-startup-image" href="launch.png"/>
    <meta name="apple-mobile-web-app-title" content="Joey Guerra"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="canonical" href="https://www.joeyguerra.comblog/2024/flow-vs-batch-based-work.html"/>
    <link rel="stylesheet" href="css/blog.css"/>
    <link rel="preload" href="HubotSans/Hubot-Sans.woff2" as="font" type="font/woff2" crossorigin>
    <style>
      @font-face {
        font-family: 'Hubot Sans';
        src:
          url('HubotSans/Hubot-Sans.woff2') format('woff2 supports variations'),
          url('HubotSans/Hubot-Sans.woff2') format('woff2-variations');
        font-weight: 200 900;
        font-stretch: 75% 125%;
      }
      html, body {
        font-family: "Hubot Sans","Hubot Sans Header Fallback",-apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
      }
      header {
        margin: 0 auto;
        width: 100%;
      }
      header a {
        display: block;
        width: 115px;
        height: 115px;
        background: url(/imgs/logo-2x.svg) no-repeat center center;
        background-color: black;
        border-radius: 50%;
      }
      header a span {
        text-indent: -9999px;
        position: absolute;
        display: block;
      }
    </style>
  </head>
  <body>
    <header>
      <a title="Joey Guerra's Blog" href="blog/">
        <span>Joey Guerra's Blog</span>
      </a>
    </header>
    <main role="main">
        <section>
          <h1>Embracing Flow-Based Work: A Path to Greater Flexibility and Efficiency</h1>
<figure>
    <img class="full-width" src="/imgs/flow-vs-batch-based-work.webp" alt="AI generated image of a girl carrying a basket, wearing a backpack, dressed in a monks long dress, walking down a curing path." />
</figure>
<p>In software development, the common approach often involves large batches of work, where teams are heads down for months and new features or changes have to wait in line. This approach can lead to long lead times (the duration between when an idea is conceived and when it’s delivered), significant delays, and a constant need for re-prioritization, often involving time consuming discussions with stakeholders. Ever heard someone say they want to “get in the flow”? There’s a way of working to do just that, which is more flexible and more efficient than the common approach.</p>
<h1>The Power of Flow-Based Work</h1>
<p>Flow-based work focuses on breaking down tasks into small, manageable pieces that can be completed (all the way into production) incrementally. This approach allows teams to continuously deliver value, remain adaptable, and quickly respond to new requests or issues as they arise. <a href="ttps://www.linkedin.com/in/jonathanrkeith/" title="Jonathan's LinkedIn Profile">Jonathan Keith</a> described, “it’s about having a team ready to start on anything at any time, without the bottlenecks that typically come with large batch processes”.</p>
<p>Imagine a scenario where a request is made for a small feature change, such as modifying the verbiage on a web page. In many teams, this request might be slotted into the next quarter’s work, or it might require a big conversation with executives to rescope and reprioritize existing commitments. But with a flow-based team, the response is different:</p>
<blockquote>
<p>We can start tomorrow if you’re ready. If not, let us know when you’ll be ready, and we will be too.</p>
</blockquote>
<p>This shift in mindset not only reduces friction but also accelerates the lead time.</p>
<h1>Small Batches [of work] vs. Large Batches: The Impact on Throughput</h1>
<p>The concept of small batches versus large batches can be likened to manufacturing processes, where the flow of work through a system is optimized by minimizing the size of the batches. Small batches enable improved flow through the system, while large batches decrease throughput. This principle applies equally to software development; where a batch is a group of items from the backlog.</p>
<p>In a flow-based system, teams are not bogged down by the need to complete large chunks of work before moving on to the next task. Instead, they can focus on small, incremental improvements that keep the momentum going. <a href="https://www.linkedin.com/in/logandell/" title="Logan's LinkedIn Profile">Logan Cannon</a> emphasizes the importance of eliminating time-consuming steps to maintain the flow, as any process that takes too long or has a high setup cost can disrupt the entire system.</p>
<h1>Balancing Flow and Batch Processes</h1>
<p>While flow-based work offers significant advantages, it’s important to recognize that some elements of batch processing may still be necessary, particularly in contexts where external constraints exist. <a href="https://www.linkedin.com/in/kijanawoodard/" title="Kijana's LinkedIn Profile">Kijana Woodard</a> shares an insightful perspective on this, reflecting on a potential client who sells on Amazon. In this case, the shipping process is inherently batched, as Amazon picks up orders once a day. The key is to find a balance between flow and batch processes, ensuring that each is used where it’s most effective.</p>
<p>For example, preparing the environment for automated tests ahead of time can be seen as a batch process that supports flow-based development. By setting up certain resources in advance, the team can maintain a steady flow of work without interruptions.</p>
<h1>The Benefits of Combining Flow and Small Batches</h1>
<p>One of the most compelling aspects of combining flow-based work with small batches is the ability to make fast adjustments to the process. As <a href="ttps://www.linkedin.com/in/jonathanrkeith/" title="Jonathan's LinkedIn Profile">Jonathan Keith</a> highlights, this approach allows teams to quickly respond to bottlenecks and adapt to new challenges without disrupting the overall workflow.</p>
<p>In practice, this means that even when a team completes a product feature before the marketing team is ready, they can use feature flags to launch the feature without causing disruptions. This level of flexibility ensures that the team remains productive and that valuable features are delivered to users as soon as possible.</p>
<h1>Conclusion</h1>
<p>Embracing flow-based work and small batches is a powerful strategy for improving efficiency and flexibility in software development. By reducing lead times, eliminating bottlenecks, and balancing flow with necessary batch processes, teams can create a more responsive and adaptable environment. This approach not only benefits the development team but also aligns with the broader goals of the business, enabling faster delivery of valuable features and a more seamless experience for stakeholders.</p>
<p>This article is based on a conversation we had on our Discord server. The reader will not that the journey towards flow-based work is ongoing, with continuous learning and adaptation. But the results — faster delivery, fewer disruptions, and a more agile team — are well worth the effort.</p>

        </section>
        <section class="tags">
          <span>#agile</span><span>#lean</span><span>#tps</span><span>#toyota production system</span><span>#software</span><span>#flexible</span>
        </section>
    </main>
    <footer>
      <small>This is my personal blog. It's a playground I use to explore my own opinions and they are very dynamic. Please keep it in mind as you read through. I hope this site makes you smile.</small>
      <a href="/blog/" title="Go to blog">
        <img src="imgs/joeysgotwings.jpeg" width="50%" alt="Joey's got wings" />
      </a>
      <a id="repo-link" href="https://gitlab.com/joey/joey.gitlab.io" title="Blog Repo">https://gitlab.com/joey/joey.gitlab.io</a>
  </footer>
</body>
</html>