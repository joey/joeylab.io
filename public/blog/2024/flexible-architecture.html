<!doctype html>
<html lang="en">
  <head>
    <meta content="text/html; charset=UTF-8" name="Content-Type"/>
    <meta charset="utf-8"/>
    <base href="../../"/>
    <!--<meta http-equiv="Content-Security-Policy" content="default-src https:">-->

    <title>Everybody wants a flexible system</title>
    <meta name="author" content="Joey Guerra"/>
    <meta name="description" content="A system that is easy to change over time. Maximize ROI. Be responsive. Competitive. Adapt to a changing environment."/>
    <meta name="image" content="https://www.joeyguerra.com/imgs/head-of-it.webp"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="publish_date" property="og:publish_date" content="Mon May 06 2024 06:52:00 GMT-0500 (Central Daylight Time)"/>

    <meta property="og:title" content="Everybody wants a flexible system"/>
    <meta property="og:image" content="https://www.joeyguerra.com/imgs/head-of-it.webp"/>
    <meta property="og:description" content="A system that is easy to change over time. Maximize ROI. Be responsive. Competitive. Adapt to a changing environment."/>
    <meta property="og:url" content="https://www.joeyguerra.comblog/2024/flexible-architecture.html"/>
    <meta property="og:type" content="summary"/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@ijoeyguerra"/>
    <meta name="twitter:creator" content="@ijoeyguerra"/>
    <meta name="twitter:title" content="Everybody wants a flexible system"/>
    <meta name="twitter:image" content="https://www.joeyguerra.com/imgs/head-of-it.webp"/>
    <meta name="twitter:description" content="A system that is easy to change over time. Maximize ROI. Be responsive. Competitive. Adapt to a changing environment."/>
    
    <link rel="apple-touch-icon" href="favicon_package_v0/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="favicon_package_v0/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="favicon_package_v0/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="favicon_package_v0/favicon-16x16.png"/>
    <link rel="manifest" href="favicon_package_v0/site.webmanifest"/>
    <link rel="mask-icon" href="favicon_package_v0/safari-pinned-tab.svg" color="#5bbad5"/>
    <meta name="msapplication-TileColor" content="#da532c"/>
    <meta name="theme-color" content="#ffffff"/>

    <link rel="shortcut icon" href="favicon_package_v0/favicon.ico"/>

    <link rel="apple-touch-startup-image" href="launch.png"/>
    <meta name="apple-mobile-web-app-title" content="Joey Guerra"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="canonical" href="https://www.joeyguerra.comblog/2024/flexible-architecture.html"/>
    <link rel="stylesheet" href="css/blog.css"/>
    <link rel="preload" href="HubotSans/Hubot-Sans.woff2" as="font" type="font/woff2" crossorigin>
    <style>
      @font-face {
        font-family: 'Hubot Sans';
        src:
          url('HubotSans/Hubot-Sans.woff2') format('woff2 supports variations'),
          url('HubotSans/Hubot-Sans.woff2') format('woff2-variations');
        font-weight: 200 900;
        font-stretch: 75% 125%;
      }
      html, body {
        font-family: "Hubot Sans","Hubot Sans Header Fallback",-apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
      }
      header {
        margin: 0 auto;
        width: 100%;
      }
      header a {
        display: block;
        width: 115px;
        height: 115px;
        background: url(/imgs/logo-2x.svg) no-repeat center center;
        background-color: black;
        border-radius: 50%;
      }
      header a span {
        text-indent: -9999px;
        position: absolute;
        display: block;
      }
    </style>
  </head>
  <body>
    <header>
      <a title="Joey Guerra's Blog" href="blog/">
        <span>Joey Guerra's Blog</span>
      </a>
    </header>
    <main role="main">
        <section>
          <h1>Everybody wants a flexible system</h1>
<p>A short story.</p>
<figure>
    <img class="full-width" src="/imgs/head-of-it.webp" alt="A woman software development manager standing in front of a team of developers working on their laptops, looking puzzled, while a consultant points to a screen displaying the blueprint of an inflexible warehouse management system." />
</figure>
<blockquote cite="https://thedesigngesture.com/flexibility-in-architecture-a-design-strategy/">
In architecture, flexibility refers to the ability of a structure or area to be modified in a reasonable manner, allowing a [system] to evolve over the long run as the user needs change to accommodate market shifts and extend the project’s life. Architecture that struggles to adapt to change runs the risk of becoming stagnant.
<a href="https://thedesigngesture.com/flexibility-in-architecture-a-design-strategy/">The Design Gesture</a>
</blockquote>
<p>At LogiFlex Solutions, a popular third-party logistics (3PL) company, Anna Stewart, the head of IT, faced a challenge. Despite implementing new software to optimize their warehouse and distribution centers, they struggled with declining efficiency. Sales and logistics teams blamed IT for the inflexible systems. To tackle the problem, Anna brought in a consultant, Julie Morgan, to uncover the root cause.</p>
<p>Julie had one observation:</p>
<blockquote>Your systems are inflexible. If you can't adapt to change, you risk stagnating. Rigid systems are at risk of stagnation. They can't pivot, limiting their competitive edge in rapidly changing markets.</blockquote>
<p>Julie soon uncovered the issues plaguing LogiFlex’s distribution centers. The off-the-shelf software weren’t designed for LogiFlex’s business model and operations. The IT team tried to customize them for their workflows, but it placed a burden on the warehouse staff to keep up. With the current market changes, their processes using the system were too slow to adapt. Something had to change.</p>
<blockquote>The problem is the rigidity of your systems,</blockquote>
<p>Julie told Anna.</p>
<blockquote>If you keep running like this, you'll lose your competitive edge.</blockquote>
<img class="full-width" src="/imgs/concerned-distribution-center.webp" alt="A distribution center with rows of inventory and a large screen displaying rigid, outdated warehouse management software, while the IT head and consultant look concerned." />
<blockquote>Inflexibility hampers ROI. When systems can't adapt, businesses struggle with obsolete tech and processes.</blockquote>
<p>A crucial customer order came in, requesting expedited product pulls over the course of the next few weeks. However, due to the inflexible processes in their warehouse software, the order couldn’t be fulfilled on time, leading to a significant loss in revenue for the customer.</p>
<blockquote>Our systems just weren't built for this,</blockquote>
<p>lamented Anna.</p>
<blockquote>We need to make changes.</blockquote>
<img class="full-width" src="/imgs/dissapointed-it-head.webp" alt="A disappointed IT head on the phone with a client, while the software development team and consultant look at standard, unfulfilled orders displayed on the screen." />
<blockquote>A flexible system maximizes ROI, ensuring it can accommodate market shifts and thrive as the landscape changes.</blockquote>
<p>Julie outlined a new vision for LogiFlex’s software development team.</p>
<blockquote>To thrive, you need flexibility in your warehouse management system. Let's adopt a modular approach, reduce batch processing, and design workflows that handle changing customer requirements.</blockquote>
<p>By restructuring the software architecture into smaller, adaptable modules, LogiFlex could better handle varying workflows and product customization.</p>
<img class="full-width" src="/imgs/reimagining-warehouse.webp" alt="A software development team reimagining their warehouse management system architecture on a large whiteboard, with modular and adaptable cells while the IT head and consultant review the new flexible setup." />
<blockquote>To future-proof, start modular: design systems that can evolve to meet new requirements and maximize business value.</blockquote>
<p>Anna and Julie immediately set about implementing changes. With the following principles to guide them:</p>
<ul>
<li>Modular Architecture: Reorganized the warehouse management system into flexible modules that could easily switch between workflows.</li>
<li>Small Batch Processing: Introduced a pull-based software architecture that minimized delays and improved responsiveness.</li>
<li>Cross-Training: Provided staff with cross-functional training to handle different software modules as demand fluctuated.</li>
</ul>
<p>Julie noted:</p>
<blockquote>
These principles are pretty common. But most implementations still result in rigid systems. With the business ending up right back where they started.
</blockquote>
<blockquote>
The trick is to apply <a href="https://www.bls.gov/careeroutlook/2020/youre-a-what/human-factors-engineer.htm" title="Human Factors Engineering Description">Human Factors Engineernig</a> discipline to <b>how</b> we apply those principles.
</blockquote>
<p>They started by taking a small team of software developers to do a <a href="https://en.wikipedia.org/wiki/Gemba">Gemba</a> walk at one of LogiFlex’s high traffic distribution centers. Their first goal was to find 1 person and make their job, and hence their life, easier. With Julie’s <a href="https://www.bls.gov/careeroutlook/2020/youre-a-what/human-factors-engineer.htm">Human Factors Engineering</a> discipline, she coached the team to ask probing questions as they shadowed people.</p>
<p>George, the intake manager, showed them what he had to do when a truck shipment first arrives. He explained that the problem starts when a customer calls for him to pull product, they don’t always want the exact quantity that’s on the skids. So in order to optimize the space in the warehouse, he has to enter into the system each quantity of product he receives so that when he pulls it, he can adjust it piece by piece. This is simple enough for a few skids, but when he gets more than 10 skids of product, it can take a while to enter each one by pieces into the system. The team picked this part of the process to start optimizing.</p>
<p>They built a custom app that enables him to scan in the Bill of Lading and fills out the form for him. All he has to do now is review and submit it into the system.</p>
<p>They continued this process, identifying bottleknecks in the whole system where they could apply technology to optimize; deliverying small solutions to small problems every week. This started to add up to big gains throughout the whole process.</p>
<img class="full-width" src="/imgs/training-session.webp" alt="A training session showing software developers learning to operate different modular systems while the consultant leads the training." />
<blockquote>Ready to adapt to tomorrow's challenges? Build systems that can evolve and stay ahead in an ever-changing world.</blockquote>
<p>Three months later, the changes transformed LogiFlex’s software development process.</p>
<p>Order fulfillment times were down by 30%, and the company could now handle customer customization efficiently. Profits were up, and Anna was proud of their newfound flexibility.</p>
<blockquote>Let's continue to build on this success,</blockquote>
<p>Anna encouraged her team.</p>
<blockquote>By staying adaptable, we'll remain competitive in any market!</blockquote>

        </section>
        <section class="tags">
          <span>#architecture</span><span>#software</span><span>#flexible</span>
        </section>
    </main>
    <footer>
      <small>This is my personal blog. It's a playground I use to explore my own opinions and they are very dynamic. Please keep it in mind as you read through. I hope this site makes you smile.</small>
      <a href="/blog/" title="Go to blog">
        <img src="imgs/joeysgotwings.jpeg" width="50%" alt="Joey's got wings" />
      </a>
      <a id="repo-link" href="https://gitlab.com/joey/joey.gitlab.io" title="Blog Repo">https://gitlab.com/joey/joey.gitlab.io</a>
  </footer>
</body>
</html>