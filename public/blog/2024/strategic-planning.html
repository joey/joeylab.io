<!doctype html>
<html lang="en">
  <head>
    <meta content="text/html; charset=UTF-8" name="Content-Type"/>
    <meta charset="utf-8"/>
    <base href="../../"/>
    <!--<meta http-equiv="Content-Security-Policy" content="default-src https:">-->

    <title>Strategic Planning - A Lesson from Kent Beck</title>
    <meta name="author" content="Joey Guerra"/>
    <meta name="description" content="Kent Beck invented Test-Driven Development (TDD), inspired from one of his dad's programming books. One of them said, "here's how to write a program. You take the input tape and you manually type in the output tape that you expect; and then, program until that's the output tape that you get from that input tape."/>
    <meta name="image" content="https://www.joeyguerra.com/imgs/851718f4-f56e-4784-a911-a8f15a71b0c5.webp"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="publish_date" property="og:publish_date" content="Sat Jun 01 2024 16:00:00 GMT-0500 (Central Daylight Time)"/>

    <meta property="og:title" content="Strategic Planning - A Lesson from Kent Beck"/>
    <meta property="og:image" content="https://www.joeyguerra.com/imgs/851718f4-f56e-4784-a911-a8f15a71b0c5.webp"/>
    <meta property="og:description" content="Kent Beck invented Test-Driven Development (TDD), inspired from one of his dad's programming books. One of them said, "here's how to write a program. You take the input tape and you manually type in the output tape that you expect; and then, program until that's the output tape that you get from that input tape."/>
    <meta property="og:url" content="https://www.joeyguerra.comblog/2024/strategic-planning.html"/>
    <meta property="og:type" content="summary"/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="@ijoeyguerra"/>
    <meta name="twitter:creator" content="@ijoeyguerra"/>
    <meta name="twitter:title" content="Strategic Planning - A Lesson from Kent Beck"/>
    <meta name="twitter:image" content="https://www.joeyguerra.com/imgs/851718f4-f56e-4784-a911-a8f15a71b0c5.webp"/>
    <meta name="twitter:description" content="Kent Beck invented Test-Driven Development (TDD), inspired from one of his dad's programming books. One of them said, "here's how to write a program. You take the input tape and you manually type in the output tape that you expect; and then, program until that's the output tape that you get from that input tape."/>
    
    <link rel="apple-touch-icon" href="favicon_package_v0/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="favicon_package_v0/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="favicon_package_v0/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="favicon_package_v0/favicon-16x16.png"/>
    <link rel="manifest" href="favicon_package_v0/site.webmanifest"/>
    <link rel="mask-icon" href="favicon_package_v0/safari-pinned-tab.svg" color="#5bbad5"/>
    <meta name="msapplication-TileColor" content="#da532c"/>
    <meta name="theme-color" content="#ffffff"/>

    <link rel="shortcut icon" href="favicon_package_v0/favicon.ico"/>

    <link rel="apple-touch-startup-image" href="launch.png"/>
    <meta name="apple-mobile-web-app-title" content="Joey Guerra"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="canonical" href="https://www.joeyguerra.comblog/2024/strategic-planning.html"/>
    <link rel="stylesheet" href="css/blog.css"/>
    <link rel="preload" href="HubotSans/Hubot-Sans.woff2" as="font" type="font/woff2" crossorigin>
    <style>
      @font-face {
        font-family: 'Hubot Sans';
        src:
          url('HubotSans/Hubot-Sans.woff2') format('woff2 supports variations'),
          url('HubotSans/Hubot-Sans.woff2') format('woff2-variations');
        font-weight: 200 900;
        font-stretch: 75% 125%;
      }
      html, body {
        font-family: "Hubot Sans","Hubot Sans Header Fallback",-apple-system,BlinkMacSystemFont,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
      }
      header {
        margin: 0 auto;
        width: 100%;
      }
      header a {
        display: block;
        width: 115px;
        height: 115px;
        background: url(/imgs/logo-2x.svg) no-repeat center center;
        background-color: black;
        border-radius: 50%;
      }
      header a span {
        text-indent: -9999px;
        position: absolute;
        display: block;
      }
    </style>
  </head>
  <body>
    <header>
      <a title="Joey Guerra's Blog" href="blog/">
        <span>Joey Guerra's Blog</span>
      </a>
    </header>
    <main role="main">
        <section>
          <h1>Strategic Planning: A Lesson from Kent Beck</h1>
<figure>
    <img class="full-width" src="/imgs/851718f4-f56e-4784-a911-a8f15a71b0c5.webp" alt="Strategic Planning" />
    <figcaption>
        Open AI generated hero image for this blog post.
    </figcaption>
</figure>
<p>Beck, the inventor of Test-Driven Development (TDD), shared some intriguing insights that can be applied to strategic planning.</p>
<p>He <a href="https://www.youtube.com/watch?v=1zaCvLVU70o">begins</a> by telling the story of how he got into testing. And recalls reading a book that describes how to write an application.</p>
<blockquote>
<p>“You take the input tape and you manually type in the output tape that you expect, and then program until that’s the output tape that you get from that input tape.”</p>
</blockquote>
<p>This simple yet powerful concept got me thinking about its application in strategic planning. Here’s how we can draw parallels:</p>
<ul>
<li><strong>Output Tape = Outcomes</strong></li>
<li><strong>Input Tape = Strategies</strong></li>
<li><strong>Program = Goals</strong></li>
</ul>
<h2>Applying TDD Principles to Strategic Planning</h2>
<p>In strategic planning, the ultimate objective is to achieve specific outcomes. These outcomes are analogous to the output tape in Beck’s analogy. The strategies we develop represent the input tape, and the goals we set are akin to the program.</p>
<ol>
<li>
<p>Define Desired Outcomes (Output Tape):
Just as you would define the expected output tape in programming, start by identifying the outcomes you aim to achieve. These could be business targets, project milestones, or other strategic objectives.</p>
</li>
<li>
<p>Develop Strategies (Input Tape):
Next, outline the strategies that will guide you towards these outcomes. These are the actions and plans that you believe will lead to your desired results.</p>
</li>
<li>
<p>Set Goals (Program):
Finally, set specific, measurable goals. These goals are the steps you will take, much like writing the program, to ensure that your strategies (input tape) produce the desired outcomes (output tape).</p>
</li>
</ol>
<p>By taking this systematic approach, inspired by TDD, you can create a clear roadmap for your strategic planning. This method helps ensure that your efforts are focused and aligned with your desired outcomes, making it easier to track progress and make adjustments as needed.</p>
<p>In essence, just as TDD helps in creating robust and reliable software, applying these principles to strategic planning can lead to more effective and successful outcomes in your projects and business endeavors.</p>

        </section>
        <section class="tags">
          <span>#strategy</span><span>#planning</span><span>#goals</span><span>#objectives</span><span>#tdd</span>
        </section>
    </main>
    <footer>
      <small>This is my personal blog. It's a playground I use to explore my own opinions and they are very dynamic. Please keep it in mind as you read through. I hope this site makes you smile.</small>
      <a href="/blog/" title="Go to blog">
        <img src="imgs/joeysgotwings.jpeg" width="50%" alt="Joey's got wings" />
      </a>
      <a id="repo-link" href="https://gitlab.com/joey/joey.gitlab.io" title="Blog Repo">https://gitlab.com/joey/joey.gitlab.io</a>
  </footer>
</body>
</html>