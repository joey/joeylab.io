# Joey Guerra's Personal Website

# Start

```sh
npm i
npm run start:local
```

# Deploy to local k8s

```sh
docker build . -t local/jbot-website:1.0.2
kubectl create secret generic discord-token --from-literal=HUBOT_DISCORD_TOKEN=<replace with token>
kubectl apply -f charts/web/deployment.yml -n default
```